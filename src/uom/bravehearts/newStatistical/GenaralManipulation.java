package uom.bravehearts.newStatistical;

import java.util.*;

public class GenaralManipulation {
	
	/**
	 * The method use for take distinct words from the candidate sentences coming from 
	 * rule based module
	 */
	public ArrayList<String> takeDistinctWordsFromCandidateSentences(ArrayList<String> candidateSentences){
		ArrayList<String> DistinctWordsSet = new ArrayList<String>();
		
		for (int i = 0; i < candidateSentences.size(); i++) {
			for (String word : candidateSentences.get(i).split(" ")) {
				if (!DistinctWordsSet.contains(word)) {
					DistinctWordsSet.add(word);
				}
			}
		}
		
		return DistinctWordsSet;
	}

}

class Count{
	String word;
	int count;
	LinkedHashSet<String> databaseSentences;
	
	/**
	 * Default constructor
	 */
	Count(){}
	
	/**
	 * The constructor used to initialize the words with the database sentences
	 */
	Count(String word, LinkedHashSet<String> databaseSentences){
		this.word = word;
		this.databaseSentences = databaseSentences;
	}
	
	/**
	 * The method use to count the words for calculating probability
	 */
	void setCount(){
		Iterator it = databaseSentences.iterator();
		while(it.hasNext()){				 
				String currentSentence = it.next().toString();
				String findStr = word;
				int lastIndex = 0;

				while(lastIndex != -1){

				    lastIndex = currentSentence.indexOf(findStr,lastIndex);

				    if(lastIndex != -1){
				        count ++;
				        lastIndex += findStr.length();
				    }
				}
		}
	}
	
	/**
	 * Used to get the count
	 */
	int getCount(){
		return count;
	}
	
	int getCount(ArrayList<String> databaseSentences,String wordPhrase){
		int wordCount = 0;
		for (int i = 0; i < databaseSentences.size(); i++) {
			String currentSentence = databaseSentences.get(i).toString();
			String findStr = wordPhrase;
			int lastIndex = 0;

			while(lastIndex != -1){

			    lastIndex = currentSentence.indexOf(findStr,lastIndex);

			    if(lastIndex != -1){
			        wordCount ++;
			        lastIndex += findStr.length();
			    }
			}
		}
		return wordCount;
	}
}
