package uom.bravehearts.newStatistical;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class PhraseModel {

	/**
	 * Used to split a sentence to phrases
	 */
	public ArrayList getPhrases(String candidateSentence){
		ArrayList<String> phrases = new ArrayList<String>();
		String pattern = "[a-zA-Z]";
		String[] splited = candidateSentence.split(pattern);
		for (int i = 0; i < splited.length; i++) {
			if (!splited[i].equals("")) {
				String tempOne = splited[i].replaceAll("\\s+$", "");
				String tempTwo = tempOne.trim();//tempOne.replaceAll("\\s+^", "");
				phrases.add(tempTwo);
			}
		}
		return phrases;
	}
	
}
