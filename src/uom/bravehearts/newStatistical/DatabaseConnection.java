package uom.bravehearts.newStatistical;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class DatabaseConnection {
	private Connection connection;
	private Statement statement;
	private ResultSet rs;
	
	/**
	 * The constructor using for establishing the connection between the database and the program
	 */
	public DatabaseConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			connection = DriverManager.getConnection("jdbc:mysql://localhost/BraveHearts2?characterEncoding=UTF-8","root","");
			statement = connection.createStatement();
		} catch (Exception e) {
			System.out.println("Error: "+e);
		}
	}
	
	/**
	 * The method uses for taking all the sentences from the database according to the words
	 * in candidate sentences.
	 */
	public LinkedHashSet<String> getTheSentences(ArrayList<String> distinctWordList){
		StringBuilder dataRetrivalQuery = new StringBuilder();
		LinkedHashSet<String> set = new LinkedHashSet<String>();
		ResultSet resultSet = null;
		try {
			for (int i = 0; i < distinctWordList.size(); i++) {
				dataRetrivalQuery
						.append("SELECT Cor_Sentence FROM corpus Where MATCH (Cor_Sentence) AGAINST");
				dataRetrivalQuery.append("('+" + distinctWordList.get(i)+ "' IN BOOLEAN MODE)");
				
				resultSet = statement.executeQuery(dataRetrivalQuery.toString());
				allTheSentences(resultSet, set);
				dataRetrivalQuery.setLength(0);
			}
		} catch (Exception e) {
			System.out.println("An exception occured from the query"+e);
		}finally{
			return set;
		}
	}
	
	
	/**
	 * The method uses for eliminating duplicate sentences taking from the database.
	 */
	private void allTheSentences(ResultSet resultSet,LinkedHashSet<String> set){
		try {
			while (resultSet.next()) {
				String sentence = resultSet.getString("Cor_Sentence");
				set.add(sentence);
			}
		} catch (Exception e) {
			System.out.println("An exception occured "+e);
		}
	}
	
	public void closeConnection(){
		if(connection != null){
			try {
	            connection.close();
	        } catch (SQLException e) {
	        	
	        }
		}
	}
}
