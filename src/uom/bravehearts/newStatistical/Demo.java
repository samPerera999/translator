package uom.bravehearts.newStatistical;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.swing.text.html.HTMLDocument.Iterator;

public class Demo {
	
	public static void main(String[] args) {
//		ArrayList<String> sentences = new ArrayList<String>();	
//		sentences.add("කමල් NP ජාවා කෘතියක් VP කියවමින් සිටියි");
//		sentences.add("කමල් NP ජාවා වෙන් කරනවෙක් VP කියවමින් සිටියි");
//		sentences.add("කමල් NP ජාවා පොතක් VP කියවමින් සිටියි ");

		
//		double[] d1 = unigramProbabilitiesOfSentences(sentences);
//		for (int i = 0; i < d1.length; i++) {
//			System.out.println(d1[i]);
//		}
//		
//		double[] d = bigramProbabilitiesOfSentences(sentences);
//		for (int i = 0; i < d.length; i++) {
//			System.out.println(d[i]);
//		}
		
		//getTheSentence();
		/*Object[] o = determineSimilerPhrases(sentences);
		for (int i = 0; i < o.length; i++) {
			System.out.println(o[i]);
		}*/
		
//		Object[] a = ditermineSimilerPhrases(sentences);
//		for (int i = 0; i < a.length; i++) {
//			System.out.println(a[i].toString());
//		}
		
//		DatabaseConnection dbConn = new DatabaseConnection();
//		GenaralManipulation manipulation = new GenaralManipulation();
//		ArrayList<String> distinctWordList = manipulation.takeDistinctWordsFromCandidateSentences(sentences);
//		LinkedHashSet<String> dbSentences = dbConn.getTheSentences(distinctWordList);
//		ArrayList<String> databaseSentences = convertLinkedHashSetToAnArrayList(dbSentences);
//		
//		new LanuguageModel().probabilityCalculationInTriGram(databaseSentences, "උතුරු නැගෙනහිර NP ප්‍රදේශවලින්");
		
	}
	
	/**
	 * Final method responsible of making the sentence
	 */
	public static String getTheSentence(ArrayList<String> sentences){
//		ArrayList<String> sentences = new ArrayList<String>();	
//		sentences.add("ක්රීඩකයෝ NP හොඳින් තරඟය  VP ක්රීඩා  කරන්නෝය ");
//		sentences.add(" නළුවා NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය ");
//		sentences.add("නාට්යකරුවා NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය ");
//		sentences.add("නිළියෝ NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය");
		double[] d1 = unigramProbabilitiesOfSentences(sentences);
		for (int i = 0; i < d1.length; i++) {
			//System.out.println(d1[i]);
		}
		
		double[] d = bigramProbabilitiesOfSentences(sentences);
		for (int i = 0; i < d.length; i++) {
			//System.out.println(d[i]);
		}
		double[] f = new double[d.length];
		for (int i = 0; i < f.length; i++) {
			f[i] = d[i]*1000;
			f[i]=f[i]+d1[i];
			//System.out.println(f[i]);
		}
		String theSentence;
		double max = f[0];
		int index = 0;
		for (int i = 1; i < f.length; i++) {
			if(max<f[i]){
				index=i;
			}
		}
		return sentences.get(index);
		//System.out.println("The correct translation is ==> "+sentences.get(index)+".");
	}
	
	 
	/**
	 * This returns an object which are having similar phrases in all the candidate sentences.
	 */
	private static Object[] determineSimilerPhrases(ArrayList<String> sentences){
		
		/*ArrayList<String> sentences = new ArrayList<String>();
		sentences.add("ක්රීඩකයෝ NP හොඳ තරඟ ක්රීඩාවේ  VP ක්රීඩා  කරන්නෝය");
		sentences.add("නළුවා NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය");
		sentences.add("නාට්යකරුවා NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය");
		sentences.add("නිළියෝ NP හොඳ තරඟ ක්රීඩාවේ VP ක්රීඩා  කරන්නෝය");*/
		
		int comparetor = new PhraseModel().getPhrases(sentences.get(0)).size();
		Object[] finalSentence = new Object[comparetor];
		Object[] firstSentence = new PhraseModel().getPhrases(sentences.get(0)).toArray();
		
		for (int i = 1; i < sentences.size(); i++) {
			Object[] currentSentence = new PhraseModel().getPhrases(sentences.get(i)).toArray();
			for (int k = 0; k < comparetor; k++) {
				String phraseOne = firstSentence[k].toString();
				String currentPhrase = currentSentence[k].toString();
				if(phraseOne.equals(currentPhrase)){
					
					finalSentence[k] = phraseOne;
				}else{
					
					finalSentence[k] = null;
				}
			}

		}
		return finalSentence;
	}

	private static Object[] ditermineSimilerPhrases(ArrayList<String> sentences){
		PhraseModel model = new PhraseModel();
		int numberOfPhrases = new PhraseModel().getPhrases(sentences.get(0)).size();
		String[][] sentencePhrases = new String[sentences.size()][numberOfPhrases];
		Object[] finalSentence = new Object[numberOfPhrases];
		Object[] firstSentence = new PhraseModel().getPhrases(sentences.get(0)).toArray();
		int[] total = new int[numberOfPhrases];
		for (int i = 0; i < total.length; i++) {
			total[i]=0;
		}
		
		for (int i = 0; i < sentencePhrases.length; i++) {
			ArrayList<String> phrases = model.getPhrases(sentences.get(i));
			for (int j = 0; j < sentencePhrases[i].length; j++) {
				sentencePhrases[i][j] = phrases.get(j);
			}
		}
		
		for (int i = 0; i < sentencePhrases.length; i++) {
			for (int j = 0; j < sentencePhrases[i].length; j++) {
				if((i+1)<sentencePhrases.length){
					if(sentencePhrases[i][j].equals(sentencePhrases[i+1][j])){
						total[j] = total[j]+1;
					}
					else{
						total[j] = total[j]+0;
					}
				}
			}
		}
		
		for (int i = 0; i < total.length; i++) {
			if(total[i]==(sentencePhrases.length-1)){
				finalSentence[i]=firstSentence[i];
			}
			else{
				finalSentence[i]="1";
			}
		}
		
//		for (int i = 0; i < sentencePhrases.length; i++) {
//			for (int j = 0; j < sentencePhrases[i].length; j++) {
//				System.out.print(sentencePhrases[i][j]+", ");
//			}
//			System.out.println();
//		}
		return finalSentence;
	}
	
	/**
	 * This method is used to modify the candidate sentences
	 * taken from Sameera and since the sentences are coming
	 * with tags.
	 */
	private static ArrayList<String> modifyCandidateSentences(ArrayList<String> sentences){
		ArrayList<String> sentencesList = new ArrayList<String>();
		for (int i = 0; i < sentences.size(); i++) {
			Object[] modifySentence = new PhraseModel().getPhrases(sentences.get(i)).toArray();
			String theSentence = "";
			for (int j = 0; j < modifySentence.length; j++) {
				theSentence = theSentence+modifySentence[j]+" ";
			}
			sentencesList.add(theSentence);
		}
		return sentencesList;
	}
	
	/**
	 * This method is using printing something to make my life easy for checking accuracy purposes. 
	 */
	private static void printDataList(ArrayList<String> somethingToBePrint){
		for (int i = 0; i < somethingToBePrint.size(); i++) {
			System.out.println(i+" "+somethingToBePrint.get(i));
		}
	}
	
	
	/**
	 * This method is using printing something to make my life easy for checking accuracy purposes.
	 */
	private static void printDataList(LinkedHashSet<String> somethingToBePrint){
		java.util.Iterator<String> it = somethingToBePrint.iterator();
		int i=0;
		while (it.hasNext()) {			
			System.out.println(i+" "+it.next());
			i++;
		}
	}
	
	/**
	 * Used to convert an linkedHashSet to linkedList.
	 * Because it is easy to work with linkedLists.
	 */
	private static ArrayList<String> convertLinkedHashSetToAnArrayList(LinkedHashSet<String> set) {
		ArrayList<String> list = new ArrayList<String>();
		java.util.Iterator<String> it = set.iterator();
		while (it.hasNext()) {
			list.add(it.next());
		}
		return list;
	}
	
	private static double[] unigramProbabilitiesOfSentences(ArrayList<String> sentences){
		DatabaseConnection dbConn = new DatabaseConnection();
		GenaralManipulation manipulation = new GenaralManipulation();
		ArrayList<String> distinctWordList = manipulation.takeDistinctWordsFromCandidateSentences(sentences);
		LinkedHashSet<String> dbSentences = dbConn.getTheSentences(distinctWordList);
		LanuguageModel language = new LanuguageModel();
		ArrayList<String> databaseSentences = convertLinkedHashSetToAnArrayList(dbSentences);
		double accumilator = 0;
		double[] probabilities = new double[sentences.size()];
		Object[] similarPhrases = ditermineSimilerPhrases(sentences); 
		for (int i = 0; i < similarPhrases.length; i++) {
			if(similarPhrases[i].toString().equals("1")){
				similarPhrases[i] = "123";
			}
		}
		PhraseModel model = new PhraseModel();
		for (int i = 0; i < sentences.size(); i++) {
			ArrayList<String> phrases = model.getPhrases(sentences.get(i));
			for (int j = 0; j < phrases.size(); j++) {
				for (int j2 = 0; j2 < similarPhrases.length; j2++) {
					if(phrases.get(j).equals(similarPhrases[j2].toString())){
						accumilator = accumilator+0;
					}
					else{
						double temp = 0;
						temp = language.probablityCalculationInUniGram(databaseSentences, phrases.get(j));
						accumilator = accumilator + temp; 
					}
				}
			probabilities[i] = accumilator;	
			}
			accumilator = 0;
		}
		dbConn.closeConnection();
		return probabilities;
	}
	
	private static double[] bigramProbabilitiesOfSentences(ArrayList<String> sentences){
		DatabaseConnection dbConn = new DatabaseConnection();
		GenaralManipulation manipulation = new GenaralManipulation();
		ArrayList<String> distinctWordList = manipulation.takeDistinctWordsFromCandidateSentences(sentences);
		LinkedHashSet<String> dbSentences = dbConn.getTheSentences(distinctWordList);
		LanuguageModel language = new LanuguageModel();
		ArrayList<String> databaseSentences = convertLinkedHashSetToAnArrayList(dbSentences);
		double accumilator = 0;
		double[] probabilities = new double[sentences.size()];
		Object[] similarPhrases = ditermineSimilerPhrases(sentences); 
		for (int i = 0; i < similarPhrases.length; i++) {
			if(similarPhrases[i]=="1"){
				similarPhrases[i] = "123";
			}
		}
		PhraseModel model = new PhraseModel();
		for (int i = 0; i < sentences.size(); i++) {
			ArrayList<String> phrases = model.getPhrases(sentences.get(i));
			for (int j = 0; j < phrases.size(); j++) {
				for (int j2 = 0; j2 < similarPhrases.length; j2++) {
					if(phrases.get(j).equals(similarPhrases[j2].toString())){
						accumilator = accumilator+0;
					}
					else{
						double temp = 0;
						temp = language.probabilityCalculationInBiGram(databaseSentences, phrases.get(j));
						accumilator = accumilator + temp; 
					}
				}
			probabilities[i] = accumilator;	
			}
			accumilator = 0;
		}
		dbConn.closeConnection();
		return probabilities;
	}
}
