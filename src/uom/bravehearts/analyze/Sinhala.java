package uom.bravehearts.analyze;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uom.bravehearts.grammer.RulesAdmin;



import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.io.StringReader;

import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
/**
 * Servlet implementation class Sinhala
 */
@WebServlet("/Sinhala")
public class Sinhala extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Analyzer start;
	RulesAdmin perform;
	AnalyzerModule am;
	
	
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sinhala() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init() throws ServletException{
    	start =new Analyzer();
    	perform= new RulesAdmin();
    	am= new AnalyzerModule();
    	
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<List>cover =new ArrayList<>();
		
		// TODO Auto-generated method stub
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html");
		PrintWriter out= response.getWriter();
		System.out.println("con path :"+request.getContextPath());
		//out.println("U+6771");
		
		String input=request.getParameter("input");
		
		try{
		
		//List<Map> analyzed=start.analyzer(input);
		List<String> analyzed=am.analyzer(input);
		cover.add(analyzed);
		//List<Map> analyzed=start.analyzer("piyumi is mongal");
		String translated= perform.startPerform(cover);
		System.out.println();
		//System.out.println("output:"+translated);
		
		
		
		
		
		
		System.out.println("input "+input);
		
		out.print(translated);
		
		}
		catch(Exception e){
			e.printStackTrace();
			out.print("ගැටළුවක් පැන නැගී ඇත. කරුණාකර වෙනත් ආදානයක් යොදා නැවත උත්සාහා කරන්න...");
		}
		
		//String s=new String(Character.toChars(0x0D86));
		//String ආ;
		//String y ="\u0D85\u0DB1\u0DD4\u0DC2\u0DCA\u0D9A";
		
		//out.println(y);
		//for(char a:Character.toChars(3462)){
			
		//out.println(a);
			
			
		//}
		
		
		
		
		

	}

}
