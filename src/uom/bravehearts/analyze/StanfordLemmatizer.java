package uom.bravehearts.analyze;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import java.util.*; 
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.ling.*; 
import edu.stanford.nlp.ling.CoreAnnotations.*; 
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;


	
	public class StanfordLemmatizer {

	    protected StanfordCoreNLP pipeline;

	    public StanfordLemmatizer() {
	        // Create StanfordCoreNLP object properties, with POS tagging
	        // (required for lemmatization), and lemmatization
	        Properties props;
	        props = new Properties();
	        props.put("annotators", "tokenize, ssplit, pos, lemma");

	        // StanfordCoreNLP loads a lot of models, so you probably
	        // only want to do this once per execution
	        this.pipeline = new StanfordCoreNLP(props);
	    }

	    public String lemmatize(String documentText)
	    {
	        List<String> lemmas = new LinkedList<String>();

	        // create an empty Annotation just with the given text
	        Annotation document = new Annotation(documentText);

	        // run all Annotators on this text
	        this.pipeline.annotate(document);

	        // Iterate over all of the sentences found
	        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	        for(CoreMap sentence: sentences) {
	            // Iterate over all tokens in a sentence
	            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	                // Retrieve and add the lemma for each word into the list of lemmas
	                lemmas.add(token.get(LemmaAnnotation.class));
	            }
	        }

	        return lemmas.get(0);
	    }
	    
	    public static void main(String[]args){
	    	
	    	StanfordLemmatizer slmt =new StanfordLemmatizer();
	    	System.out.println("lemma :"+ slmt.lemmatize("cats"));
	    }
	    

}
