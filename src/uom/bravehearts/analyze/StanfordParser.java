package uom.bravehearts.analyze;


import java.io.IOException;
import java.io.StringReader;
import java.util.*;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

import edu.stanford.nlp.trees.GrammaticalStructure;

class StanfordParser {

  /** This example shows a few more ways of providing input to a parser.
   *
   *  Usage: ParserDemo2 [grammar [textFile]]
   */
  public static Tree stanfordParser(String arg) throws IOException {
    String grammar = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
    String[] options = { "-maxLength", "80", "-retainTmpSubcategories" };
    LexicalizedParser lp = LexicalizedParser.loadModel(grammar, options);
    TreebankLanguagePack tlp = lp.getOp().langpack();
    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();

   
    

    // This method turns the String into a single sentence using the
    // default tokenizer for the Tree bank Language Pack.
   // String sent3 = "Polymorphism is the ability of an object to take many forms";
    System.out.println("Sentence: "+arg);
   // lp.parse(arg).pennPrint();
    return lp.parse(arg);
    
    
  //  System.out.println(lp.parse(sent3));
  }

  private StanfordParser() {} // static methods only

}