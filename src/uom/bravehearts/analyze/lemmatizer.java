package uom.bravehearts.analyze;

import java.util.*; 
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.ling.*; 
import edu.stanford.nlp.ling.CoreAnnotations.*; 
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;



public class lemmatizer {
	
	
	
		  public static void main(String[] args)
		  {
			  Properties props = new Properties(); 
			  StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		 
		  props.put("annotators", "tokenize, ssplit, pos, lemma"); 
		  pipeline = new StanfordCoreNLP(props, false);
		  String text ="cats"; /* the string you want */; 
		  Annotation document = pipeline.process(text);  
		  for(CoreMap sentence: document.get(SentencesAnnotation.class)) {    
		    for(CoreLabel token: sentence.get(TokensAnnotation.class)) {       
		    String word = token.get(TextAnnotation.class);      
		    String lemma = token.get(LemmaAnnotation.class); 
		    System.out.println("lemmatized version :" + lemma);
		    } } }
	
	
	
	
	
	
	

}
