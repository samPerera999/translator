package uom.bravehearts.analyze;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;

import edu.stanford.nlp.trees.Tree;

public class Analyzer {

	/**
	 * @param args
	 */
	

	public List analyzer(String text) {
		
		List<List> sentences = new ArrayList<List>(); // to store full sentence
		List<Map> finalphrases = new ArrayList<Map>();// to store the phrases of the
														// sentence
		Map<String, String> svo = new LinkedHashMap<String, String>();// to store
																		// the
																		// subject
																		// ,verb and
																		// object
																		// phrases

		Tree nlpTree = null;
		List<List> phrases = new ArrayList<List>();
		List<String> words = null; // to store phrases by analyzing nlp tree
		;
		try {

			nlpTree = StanfordParser.stanfordParser(text); // this will return
															// the parsed nlp
															// tree of a
															// sentnece

		} catch (IOException e) {

			e.printStackTrace();
		}

		// traverse the nlp tree and storing values
		for (Tree subtree : nlpTree) {
			String value = (subtree.label().value());

			if (value.equalsIgnoreCase("~")) { // dividing the sentence into
												// phrases

				if (words != null) { // add words to the phrases list
					phrases.add(words);
					words = null;
					words = new ArrayList<String>();
				} else {

					words = new ArrayList<String>();
				}

			} else {

				if (words == null) { // create a new list if it is null
					words = new ArrayList<String>();

				}

				words.add(value); // add words from nlp tree to the words list
			}

		}
		// this if condition will add the final phrase of a sentence to the
		// phrase list because above if condition will not perform at the last
		// phrase
		if (words != null) {
			phrases.add(words);
		}

		int listCount = 0; // to determine the position of the phrases list

		for (int x = 0; x < phrases.size(); x++) {

			List s = phrases.get(listCount); // this will get the phrases one by
												// one

			// this will check whether this only contain nlp tree keywords or
			// keywords with words.this will prevent the creating unnecessary
			// keyword phrases
			if (!(s.contains("NP") || s.contains("VP"))) {

				if (listCount < phrases.size()) {
					s.addAll(phrases.get((listCount + 1))); // if this phrase
															// contain only
															// keywords add next
															// phrase to the
															// current phrase
					phrases.remove((listCount + 1)); // then remove the next
														// phrase because it
														// will be a duplicate
														// phrase
				}

			} else {

				listCount++;

			}

		}

		int count = 1;
		// this will get the phrases one by one and determine the Subject verb
		// and object phrases
		for (int x = 0; x < phrases.size(); x++) {
			int y = 0;

			System.out.println("Phrases :" + count++);

			List display = phrases.get(x);
			int size = display.size();

			StringBuffer subject = new StringBuffer();
			StringBuffer verb = new StringBuffer();
			StringBuffer object = new StringBuffer();

			for (int c = 0; c < size; c++) {
				System.out.print(" " + display.get(c));
			}
			System.out.println();

			// finding subject phrase
			for (int s = 0; s < size; s++) {
				if (display.get(s).equals("VP")) {

					break;
				} else {
					subject.append(" " + (String) display.get(s));

					y++;
				}
			}

			// finding verb phrase
			for (int z = y; z < size; z++) {

				if (size > (y + 3)) {

					if (((display.get(y + 2).equals("is")
							|| display.get(y + 2).equals("are")
							|| display.get(y + 2).equals("was") || display.get(
							y + 2).equals("were")))) {

						if (!(display.get(y + 3).equals("VP"))) {

							break;
						}
					}

				}

				if (display.get(y).equals("NP")) {
					;

					break;
				}

				else {

					verb.append(" " + (String) display.get(y));

					y++;
				}

			}

			// finding the object phrase

			for (int z = y; z < size; z++) {

				object.append(" " + (String) display.get(y));

				y++;

			}
			System.out.println("");
			System.out.println("SVO before manipulation");
			
			System.out.println("Subject "+subject.toString());
			System.out.println("verb "+verb.toString());
			System.out.println("object "+object.toString());
			System.out.println("");
			
			

			// add svo into the svo map
			svo.put("subject", subject.toString());
			svo.put("verb", verb.toString());
			svo.put("object", object.toString());

			subject = new StringBuffer();
			verb = new StringBuffer();
			
			finalphrases.add(svo);
			svo = new LinkedHashMap<String, String>();

		}
		// svo map add to the respective phrases list
		
		return finalphrases;

	}

}
