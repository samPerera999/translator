package uom.bravehearts.statisticalmt;

import java.util.ArrayList;
import java.util.List;

public class SentencePattern {

	public static void main(String args[]) {
		String s1 = "නිමල්ට ජාවා පොතක් අවශ්‍යයි";
		String s2 = "නිමල්ට ජාවා කලින් වෙන්කිරීමක් අවශ්‍යයි";
		// wordSeparate(s1);

		System.out.println("Sentence" + s1);
		String[] splitWords = sentenceSpliter(s1);
		String[] ontologyWordArray;
		String[] newSentence = new String[splitWords.length];
		StringBuilder builder1 = new StringBuilder();
		for (int i = 0; i < splitWords.length; i++) {
			System.out.println("word" + " " + i +" "+ splitWords[i]);
			ontologyWordArray = ontologyData(splitWords[i]);

			for (int t = 0; t < ontologyWordArray.length; t++) {
				// newSentence[i] = newSentence[i]+ splitWords[i] + "/" +
				// ontologyWordArray[t];

				builder1.append(ontologyWordArray[t]);
				builder1.append("/");
			}
			builder1.append(splitWords[i]);
			builder1.append(" ");
		}

		System.out.println("New Sentence" + " " + builder1.toString());

		// String k = SentencePattern.getUnicodeValue(s1);
		// wordSeparate(k);

	}
/*
	public static String getUnicodeValue(String line) {

		String hexCodeWithLeadingZeros = "";
		String newUnicodeVal = "";
		try {
			for (int index = 0; index < line.length(); index++) {
				String hexCode = Integer.toHexString(line.codePointAt(index))
						.toUpperCase();

				String hexCodeWithAllLeadingZeros = "0000" + hexCode;
				String temp = hexCodeWithAllLeadingZeros
						.substring(hexCodeWithAllLeadingZeros.length() - 4);
				hexCodeWithLeadingZeros += ("\\u" + temp);
			}
			return hexCodeWithLeadingZeros;
		} catch (NullPointerException nlpException) {
			return hexCodeWithLeadingZeros;
		}
	}

	public static String getNormalString(String unicodeString) {
		String str = unicodeString.split(" ")[0];
		str = str.replace("\\", "");
		String[] arr = str.split("u");
		String text = "";
		for (int i = 1; i < arr.length; i++) {
			int hexVal = Integer.parseInt(arr[i], 16);
			text += (char) hexVal;
		}
		return text;
	}

	public static void wordSeparate(String sentence) {
		String[] word = sentence.split("u0020");
		for (int j = 0; j < word.length; j++) {
			String sinhalaWord = getNormalString(word[j]);

			System.out.println("word " + j + " " + sinhalaWord);
			String[] ontologyWordArray = ontologyData(sinhalaWord);
			System.out.println("Ontology words......");
			for (int k = 0; k < 3; k++) {
				System.out.println(ontologyWordArray[k]);
			}
		}

	}
*/
	public static String[] sentenceSpliter(String sentence) {

		String[] sinhalaWords = sentence.split("\\s+");

		return sinhalaWords;
	}

	public static String[] ontologyData(String word) {
		String wordReceive = word;
		String[] wordArray = null;
		if (wordReceive.equals("නිමල්ට")) {
			wordArray = new String[] { };
		}
		if (wordReceive.equals("ජාවා")) {
			wordArray = new String[] { "C#", ".Net", "python" };
		}
		if (wordReceive.equals("පොතක්")) {
			wordArray = new String[] { "සඟරාව" };
		}
		if (wordReceive.equals("අවශ්‍යයි")) {
			wordArray = new String[] { "වුවමනා" };
		}

		return wordArray;
	}

}
