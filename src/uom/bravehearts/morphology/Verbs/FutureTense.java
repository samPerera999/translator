package uom.bravehearts.morphology.Verbs;

import java.util.ArrayList;
import java.util.List;

import uom.bravehearts.morphology.MorfRule;
import uom.bravehearts.morphology.Passive.PassiveFuSi;
import uom.bravehearts.passiveVoice.PassiveIdentify;
//FUTURE PASSIVE SENTNCE IS WORKING
public class FutureTense implements MorfRule {

	public List performRule(List<String> phrase) {
		
		int phraseSize = phrase.size();
		SubjectIdentifier idnty = new SubjectIdentifier();

		System.err.println("VerbFinalizer for Future tense executed...");
		System.out.println("verbFinalizer Future tense input: " + phrase);
		{

			int count = 0;
			int countNxt = 1;
			String wordToChange = "";

			String out = idnty.identifySub(phrase);
			// call method to identify the subject
			out = out.trim();
			System.out.println("GOT THE OUTPUT IS..........." + out);
			for (int x = 0; x <= phraseSize-1; x++) {
				
				String verbToArrange = "";
				ArrayList<Character> verbPast = new ArrayList<Character>();
				
				String tenseTag = phrase.get(phraseSize-1).trim();
				PassiveFuSi passive = new PassiveFuSi();
					if(PassiveIdentify.boolPassive && "PaFuSi".equals(tenseTag)){ 
			 									
					System.out.println("Passive loop executed");
					phrase = passive.passiveVerbHandler(phrase);
					//return 	phrase;
					break;
					}
				
				if ((phrase.get(x).equals("VP")) && (x + 2 <= phraseSize)
						&& (phrase.get(x + 1).equals("VBG"))) {
					String verb = (phrase.get(x + 2));
					verbToArrange = ((phrase.get(x + 2).trim()));
					for (int z = 0; z < verbToArrange.length(); z++) {
						verbPast.add(verbToArrange.charAt(z));

					}

					for (int z = 0; z < verbToArrange.length(); z++) {
						if ((z == verbPast.size() - 1)
								&& (verbPast.get(z).equals('ා'))
								&& (verbPast.get(z - 1).equals('ව'))
								&& (verbPast.get(z - 2).equals('න'))) {
							verbPast.set(z, '්');
							verbPast.add(z, 'න');
							verbPast.set(z - 1, 'ි');
							verbPast.set(z - 2, 'ම');

							System.out.println("");
							String WordToPrint = verbPast.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phraseSize - 1; index++) {
								if (phrase.get(index).equals(verb)) {
									phrase.set(index, WordToPrint);
								}
							}
						}
					}

				}

				if ((phrase.get(x).equals("VP")) && (x + 2 <= phraseSize)) {
					String verb = (phrase.get(x + 2));
					verbToArrange = ((phrase.get(x + 2).trim()));

					for (int sub = (x + 2); sub > 0; sub--) {
						if ((phrase.get(sub).equals("sbjBoundry")))

						{
							wordToChange = ((phrase.get(sub - 1)).trim());
							System.out.println("GOT THE OUTPUT IS..........."
									+ wordToChange);
							break;

						}

					}

					// change the base verb into past tense
					for (int z = 0; z < verbToArrange.length(); z++) {
						verbPast.add(verbToArrange.charAt(z));

					}

					for (int z = 0; z < verbToArrange.length(); z++) {
						if ((z == verbPast.size() - 1)
								&& (verbPast.get(z).equals('ා'))
								&& (verbPast.get(z - 1).equals('ව'))
								&& (verbPast.get(z - 2).equals('න'))) {
							if ((verbPast.get(z - 3).equals('ි'))) {// char is
																	// "ි"
																	// before
																	// "..නවා"
																	// +ී
																	// (අරිනවා-
																	// අරීවී)

								verbPast.remove(z);
								verbPast.set(z - 3, 'ී');
								verbPast.set(z - 2, 'ව');
								verbPast.set(z - 1, 'ි');

								System.out.println("");
								String WordToPrint = verbPast.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phraseSize - 1; index++) {
									if (phrase.get(index).equals(verb)) {
										phrase.set(index, WordToPrint);

									}
								}
							} else if ((verbPast.get(z - 3).equals('ෙ'))) {// char
																			// is
																			// "ෙ"
																			// before
																			// "..නවා"
																			// +ේ
																			// (කැපෙනවා-
																			// කැපේවි)

								verbPast.remove(z);
								verbPast.set(z - 3, 'ේ');
								verbPast.set(z - 2, 'ව');
								verbPast.set(z - 1, 'ි');

								System.out.println("");
								String WordToPrint = verbPast.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phraseSize - 1; index++) {
									if (phrase.get(index).equals(verb)) {
										phrase.set(index, WordToPrint);

									}
								}
							}

							else if ((verbPast.get(z - 3).equals('ා'))) {// char
																			// is
																			// "ෙ"
																			// before
																			// "..නවා"
																			// +just
																			// remove
																			// "නවා"
																			// and
																			// add
																			// "වි"
																			// (පානවා-
																			// පාවි)

								verbPast.remove(z);
								verbPast.set(z - 2, 'ව');
								verbPast.set(z - 1, 'ි');

								System.out.println("");
								String WordToPrint = verbPast.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phraseSize - 1; index++) {
									if (phrase.get(index).equals(verb)) {
										phrase.set(index, WordToPrint);

									}
								}
							}

							else if ((verbPast.get(z - 3).equals('එ'))) {
								// char is "එ"  before "..නවා" +just remove "නවා" and add "වි" (එනවා-ඒවි)
																			

								verbPast.remove(z);
								verbPast.set(z - 2, 'ව');
								verbPast.set(z - 1, 'ි');
								verbPast.set(z - 3, 'ඒ');

								System.out.println("");
								String WordToPrint = verbPast.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phraseSize - 1; index++) {
									if (phrase.get(index).equals(verb)) {
										phrase.set(index, WordToPrint);

									}
								}
							}

							else {// char is just a letter(ව/ප/න..etc) +"ා"
									// (කරනවා- කරාවි)

								verbPast.set(z, 'ි');
								verbPast.set(z - 1, 'ව');
								verbPast.set(z - 2, 'ා');

								System.out.println("");
								String WordToPrint = verbPast.toString()
										.replaceAll(", |\\[|\\]", "");

								for (int index = 0; index <= phraseSize - 1; index++) {
									if (phrase.get(index).equals(verb)) {
										phrase.set(index, WordToPrint);

									}
								}
							}
						}
					}
				}
			}
		}
		
	if(!(PassiveIdentify.boolPassive &&  phrase.get(phraseSize-1).trim().equals("PaFuSi"))){	
		System.err.println("Future Tense output :" + phrase);
		System.out.println("Future tense continoues executed...");
		System.out.println("Future tense continoues input: " + phrase);
		{

			int count = 0;
			String wordToChange = "";

			for (int x = 0; x <= phraseSize - 1; x++) {

				if ((phrase.get(x).equals("NP") || phrase.get(x).equals("VP"))) {
					count++;
				}

				if ((count <= 2)
						&& ((phrase.get(x).equals("NP")) || ((phrase.get(x)
								.equals("VP"))))) {
					if (count == 2 && ((phrase.get(x).equals("NP")))) {
						wordToChange = ((phrase.get(x - 1)));
					}

					else if (count == 2 && ((phrase.get(x).equals("VP")))) {
						wordToChange = ((phrase.get(x - 1)));
					}

				}

				String verbToArrange = "";
				ArrayList<Character> verbHandler = new ArrayList<Character>();
				if ((phrase.get(x).equals("VP")) && (x + 2 <= phraseSize)
						&& (phrase.get(x + 1).equals("MD"))) {
					{
						// if(((phrase.get(x+2)).equals("be")))
						if (phrase.get(x + 2).equals("will")) {
							if (phrase.get(x + 5).equals("වීම")) {
								phrase.remove(x + 5);
							}

							verbToArrange = "සිටීවි";
							for (int index = 0; index <= phraseSize - 1; index++) {
								if (phrase.get(index).equals("will"))

								{
									{
										for (int y = 0; y <= phraseSize - 1; y++) {
											if ((phrase.get(y).equals("VP"))
													&& (y + 2 <= phraseSize)
													&& (phrase.get(y + 1)
															.equals("VBG"))) {
												phrase.add(verbToArrange);

											}
										}
									}

								}
							}
						}

						else {
							verbToArrange = (phrase.get(x + 2));

						}
					}

					for (int z = 0; z < verbToArrange.length(); z++) {
						verbHandler.add(verbToArrange.charAt(z));
					}
					// System.out.println("verb to array"+verbHandler);

					// CHANGE THE VERB WHEN THE SUBJECT IS (I)
					for (int z = 0; z < verbToArrange.length(); z++) {
						if ((z == verbHandler.size() - 1)
								&& (verbHandler.get(z) == 'ි')
								&& (verbHandler.get(z - 1) == 'ව')) {

							System.out.println("");
							String WordToPrint = verbHandler.toString()
									.replaceAll(", |\\[|\\]", "");

							for (int index = 0; index <= phraseSize - 1; index++) {
								if (phrase.get(index).equals(verbToArrange)) {
									phrase.set(index, WordToPrint);

								}
							}
						}

					}
				}
			}
		}
      }

		System.out.println("verbFinalizer Future tense output :" + phrase);
		return phrase;
	}
}
