package uom.bravehearts.morphology.Passive;

import java.util.ArrayList;
import java.util.List;


public class PassivePaSi  {

	
	public List<String> passiveVerbHandler (List<String> phrase) {

				System.out.println("Passive Simple Past Class executed ...");
				System.out.println("Passive Simple Past Class input: " + phrase);
				int lastIndex=phrase.size() - 1;
				String passiveVerbToArrange="";
				ArrayList<Character> passiveVerb=new ArrayList<Character>();
				
				
			if (phrase.get(lastIndex).trim().equals("PaPaSi")){
				
				System.out.println("PaPaSi loop executed");
				passiveVerbToArrange = phrase.get(lastIndex-1).trim();
				if(passiveVerbToArrange.trim().equals("කෑවා")){
					passiveVerbToArrange = "කනු ලැබ තිබේ";
					phrase.set(lastIndex-1,passiveVerbToArrange);
					
				}
				for(int z=0; z<passiveVerbToArrange.length(); z++)
					{	
						passiveVerb.add(passiveVerbToArrange.charAt(z));
					}
				
				//
				int passiveVerbsize =passiveVerb.size()-1;
				for(int z=passiveVerbsize; z>=0; z--){
					
					//
					
				if((z==passiveVerb.size()-1)&&(passiveVerb.get(z)=='ා')&& (passiveVerb.get(z-1)=='ව')&& (passiveVerb.get(z-2)=='න')&&(!(passiveVerb.get(z-3)=='ේ'))) 
					{ //  
					
						if(passiveVerb.get(z-3).equals('්'))
						{
							passiveVerb.remove(z-3);
							passiveVerb.remove(z-4);
							z = z-2;
						}
						
						passiveVerb.set(z-1,'ු');
						passiveVerb.set(z,' ');
						passiveVerb.add(z+1,'ල');
						passiveVerb.add(z+2,'ැ');
						passiveVerb.add(z+3,'බ');
						passiveVerb.add(z+4,' ');
						passiveVerb.add(z+5,'ත');
						passiveVerb.add(z+6,'ි');
						passiveVerb.add(z+7,'බ');
						passiveVerb.add(z+8,'ේ');
						
						
						System.out.println("");
						String WordToSet = passiveVerb.toString().replaceAll(", |\\[|\\]", "");
						
						System.out.println("new passiveVerb: "+passiveVerb);
						
						System.out.println("WordToPrint"+WordToSet);
						phrase.set(lastIndex-1,WordToSet);
						System.out.println("PaPrSi loop output : "+phrase);
						break;
						}
				if((z==passiveVerb.size()-1)&&(passiveVerb.get(z)=='ා')&& (passiveVerb.get(z-1)=='ව')&& (passiveVerb.get(z-2)=='න')&&(passiveVerb.get(z-3)=='ේ')) 
				{ //
					System.out.println("before remove z: "+z);
					
					passiveVerb.remove(z);
					passiveVerb.remove(z-1);
					passiveVerb.remove(z-2);
					passiveVerb.remove(z-3);
					z= z-4;
					
					
					passiveVerb.add(z+1,'ෙ');
					passiveVerb.add(z+2,'න');
					passiveVerb.add(z+3,'ෙ');
					passiveVerb.add(z+4,'න');
					passiveVerb.add(z+5,'ු');
					passiveVerb.add(z+6,' ');
					passiveVerb.add(z+7,'ල');
					passiveVerb.add(z+8,'ැ');
					passiveVerb.add(z+9,'බ');
					passiveVerb.add(z+10,' ');
					passiveVerb.add(z+11,'ත');
					passiveVerb.add(z+12,'ි');
					passiveVerb.add(z+13,'බ');
					passiveVerb.add(z+14,'ේ');
					
					System.out.println("");
					String WordToSet = passiveVerb.toString().replaceAll(", |\\[|\\]", "");
					
					//
					phrase.set(lastIndex-1,WordToSet);
					System.out.println("PaPrSi loop output : "+phrase);
					break;
						
					}
						
					}
			}  
			
		return phrase;
	}

	
		
}
