package uom.bravehearts.morphology.Passive;

import java.util.ArrayList;
import java.util.List;


public class PassivePrSi  {

	
	public List<String> passiveVerbHandler (List<String> phrase) {

				System.out.println("PassivePrSi Class executed ...");
				int lastIndex=phrase.size() - 1;
				String passiveVerbToArrange="";
				ArrayList<Character> passiveVerb=new ArrayList<Character>();
				
				
			if (phrase.get(lastIndex).trim().equals("PaPrSi")){
				
				System.out.println("PaPrSi loop executed");
				passiveVerbToArrange = phrase.get(lastIndex-1).trim();
				
				for(int z=0; z<passiveVerbToArrange.length(); z++)
					{	
						passiveVerb.add(passiveVerbToArrange.charAt(z));
					}
				
				System.out.println("passiveVerb array"+passiveVerb);
				int passiveVerbsize =passiveVerb.size()-1;  
				for(int z=0; z<=passiveVerbsize; z++){
					
					///
					
				if((z==passiveVerb.size()-1)&&(passiveVerb.get(z)=='ා')&& (passiveVerb.get(z-1)=='ව')&& (passiveVerb.get(z-2)=='න')&&(!(passiveVerb.get(z-3)=='ේ'))) 
					{  
					
						if(passiveVerb.get(z-3).equals('්'))
						{
							passiveVerb.remove(z-3);
							passiveVerb.remove(z-4);
							z = z-2;
						}
						
						passiveVerb.set(z-1,'ු');
						passiveVerb.set(z,' ');
						passiveVerb.add(z+1,'ල');
						passiveVerb.add(z+2,'බ');
						passiveVerb.add(z+3,'ය');
						passiveVerb.add(z+4,'ි');
						
						System.out.println("");
						String WordToSet = passiveVerb.toString().replaceAll(", |\\[|\\]", "");
						
						///
						phrase.set(lastIndex-1,WordToSet);
						System.out.println("PaPrSi loop output : "+phrase);
						break;
						}
				if((z==passiveVerb.size()-1)&&(passiveVerb.get(z)=='ා')&& (passiveVerb.get(z-1)=='ව')&& (passiveVerb.get(z-2)=='න')&&(passiveVerb.get(z-3)=='ේ'))
				{
					///
					
					passiveVerb.remove(z);
					passiveVerb.remove(z-1);
					passiveVerb.remove(z-2);
					passiveVerb.remove(z-3);
					z= z-4;
					///
					
					passiveVerb.add(z+1,'ෙ');
					passiveVerb.add(z+2,'න');
					passiveVerb.add(z+3,'ෙ');
					passiveVerb.add(z+4,'න');
					passiveVerb.add(z+5,'ු');
					passiveVerb.add(z+6,' ');
					passiveVerb.add(z+7,'ල');
					passiveVerb.add(z+8,'බ');
					passiveVerb.add(z+9,'ය');
					passiveVerb.add(z+10,'ි');
					
					System.out.println("");
					String WordToSet = passiveVerb.toString().replaceAll(", |\\[|\\]", "");
					
					///
					phrase.set(lastIndex-1,WordToSet);
					System.out.println("PaPrSi loop output : "+phrase);
					break;
						
					}
						
					}
			}  // PaPrSi loop ends


		return phrase;
	}

	
		
}
