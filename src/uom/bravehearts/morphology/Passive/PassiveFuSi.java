package uom.bravehearts.morphology.Passive;

import java.util.ArrayList;
import java.util.List;

public class PassiveFuSi {
	public List<String> passiveVerbHandler (List<String> phrase) {

		// 
		System.out.println("Passive Simple Future Class executed ...");
		System.out.println("Passive Simple Future Class input: " + phrase);
		int lastIndex=phrase.size() - 1;
		String passiveVerbToArrange="";
		ArrayList<Character> passiveVerb=new ArrayList<Character>();
		
		for (int x = 0; x <= lastIndex; x++) {
			if (phrase.get(x).trim().equals("VP")&&phrase.get(x+1).trim().equals("VBN")&&phrase.get(lastIndex).trim().equals("PaFuSi")){
				passiveVerbToArrange = phrase.get(x+2).trim();
				//
				
				if(passiveVerbToArrange.trim().equals("ගේනවා")){
					passiveVerbToArrange = "ගෙනෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				
				if(passiveVerbToArrange.trim().equals("පේනවා")){
					passiveVerbToArrange = "පෙනෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				
				if(passiveVerbToArrange.trim().equals("ගන්නවා")){
					passiveVerbToArrange = "ගැනෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
		
				else if(passiveVerbToArrange.trim().equals("වෙනවා")){
					passiveVerbToArrange = "වෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("කරනවා")){
					passiveVerbToArrange = "කෙරෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("තනනවා")){
					passiveVerbToArrange = "තැනෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("නගිනවා")){
					passiveVerbToArrange = "නැගෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("කියවනවා")){
					passiveVerbToArrange = "කියවෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("ලියනවා")){
					passiveVerbToArrange = "ලියවෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else if(passiveVerbToArrange.trim().equals("ලබනවා")){
					passiveVerbToArrange = "ලැබෙනු ඇත";
					phrase.set(x+2,passiveVerbToArrange);
					break;
				}
				else{
					for(int z=0; z<passiveVerbToArrange.length(); z++)
					{	
						passiveVerb.add(passiveVerbToArrange.charAt(z));
					}
				
				//
				int passiveVerbsize =passiveVerb.size()-1;  
				for(int z=0; z<=passiveVerbsize; z++){
				if((z==passiveVerb.size()-1)&&(passiveVerb.get(z)=='ා')&& (passiveVerb.get(z-1)=='ව')&& (passiveVerb.get(z-2)=='න')&&(!(passiveVerb.get(z-3)=='ේ'))) 
				{ 
				
					if(passiveVerb.get(z-3).equals('්'))
					{
						passiveVerb.remove(z-3);
						passiveVerb.remove(z-4);
						z = z-2;
					}
					
					passiveVerb.set(z-1,'ු');
					passiveVerb.set(z,' ');
					passiveVerb.add(z+1,'ඇ');
					passiveVerb.add(z+2,'ත');
				
					
					System.out.println("");
					String WordToSet = passiveVerb.toString().replaceAll(", |\\[|\\]", "");
					
					
					phrase.set(x+2,WordToSet);
					System.out.println("PaPrSi loop output : "+phrase);
					break;
					}
				}
			}
				
		}
		}
	return phrase;
  }
}
