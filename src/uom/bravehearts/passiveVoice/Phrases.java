package uom.bravehearts.passiveVoice;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Phrases {
	public static List<List> IdentifyPhrases(ArrayList<List> newArrayList,List<String> nlpKeySrore){
		
		System.err.println("Phrases(passive) executed....");
		System.out.println("Phrases(passive) input :"+newArrayList);
		
		for (List<String> manlist : newArrayList) {
			
							
			for (String nlpKey : nlpKeySrore) {

				if(!("NP".equals(nlpKey)||"VP".equals(nlpKey))){
					manlist.remove(nlpKey);
				}
			
			}
			
			int listSize = manlist.size();			
			for(int i=0;i<listSize-1;i++){
				String fTag = manlist.get(i);
				String sTag = manlist.get(i+1);
				if((fTag.equals("NP")||fTag.equals("VP"))
						&& (sTag.equals("NP")||sTag.equals("VP"))){
					
					manlist.remove(i+1);
					i--;
					listSize--;
				}
			}
			int vpFreq = Collections.frequency(manlist, "VP");
			int newlistSize = manlist.size()-1;
			if(vpFreq>=2){
				for(int i=newlistSize;i>=1;i--){
					if(manlist.get(i).equals("VP")){
						manlist.remove(i);
						break;
					}
				}
			}
		}
		
		System.out.println("Phrases(passive) output :"+newArrayList);
		return newArrayList;
		
	}

	

	
}
