package uom.bravehearts.passiveVoice;

import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.util.ArrayUtils;


public class ReduceCandidates {
	public static String[] ReduceMeanings(String[] array,String lastTag)
	{
		
		System.err.println("ReduceCandidates executed....");
		System.out.println("ReduceCandidates input : "+array+" Last tag "+lastTag);
		
		char lastChar;		
		char beforeLastChar = 'a'; 
		char befBeforeLastChar = 'n';
		char twoBeforeLastChar = 'n';
		//String previousTag = lastTag.toString();
		//String keyWord = keyVal;
		//String[] newWordList = {"klo"};
		ArrayList<String> meanings = new ArrayList<String>();
		int wordArraySize = array.length;
		for(int i=0;i<wordArraySize;i++)
		{
			int wordLength = array[i].trim().length();
			
			String word = array[i].trim();
			
			//if (wordLength>=2) 
			//{
				lastChar = word.charAt(wordLength-1);
				if (wordLength>=3) {
					beforeLastChar = word.charAt(wordLength - 2);
					befBeforeLastChar = word.charAt(wordLength-3);
				}
				
				if (wordLength>=4) {
					twoBeforeLastChar = word.charAt(wordLength - 4);					
				}
				
				try {
					if("NN".equals(lastTag)||"NNS".equals(lastTag)||"DT".equals(lastTag)||"PRP".equals(lastTag)
							||"JJ".equals(lastTag)||"CC".equals(lastTag)||"IN".equals(lastTag)||"CD".equals(lastTag)
							||"PRP$".equals(lastTag)||"FW".equals(lastTag)||"NNP".equals(lastTag)||"RB".equals(lastTag))
					{
						if (!(lastChar == 'ා' && beforeLastChar == 'ව' && befBeforeLastChar == 'න' )
								||(lastChar == 'a'||lastChar == 'n')) 
						{
							meanings.add(array[i]);
						}
									
					}
					
					if("VBZ".equals(lastTag)||"VBD".equals(lastTag)||"VBN".equals(lastTag)||"VBG".equals(lastTag)
							||"VBP".equals(lastTag)||"VB".equals(lastTag))
					{
						if ((lastChar == 'ා' && beforeLastChar == 'ව'&&  befBeforeLastChar == 'න' )
								||(beforeLastChar == 'ා'&&befBeforeLastChar=='ව' && twoBeforeLastChar== 'න' )) 
						{
							meanings.add(array[i]);
						}
									
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			//}
			
			
		}
		
		String[] newWordList = new String[meanings.size()];
		newWordList = meanings.toArray(newWordList);
		
		System.out.println("ReduceCandidates output :"+newWordList);
		return newWordList;
		
	}
	
}
