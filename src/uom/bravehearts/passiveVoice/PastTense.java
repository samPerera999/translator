package uom.bravehearts.passiveVoice;
import java.util.ArrayList;
import java.util.List;

public class PastTense {
	public static String verbToChange;
	public static String pastMorphology(ArrayList<String> phrase, int a){
		
		System.err.println("PastTense(passive) executed....");
		System.out.println("PastTense(passive) input :"+phrase);
		
		String verbToChange01;
		System.out.println(phrase.size());
		String passiveTense = PassiveIdentify.tense;
		int subBoundryIndex = phrase.indexOf("sbjBoundry");
		
		if (passiveTense == "PaPaSi") //verb to modify for past simple passive voice
		{
			for (int j = a; j < phrase.size(); j++) {
				if ((phrase.get(j).trim().equals("VP")) && ((j + 2) < phrase.size())
						&& (phrase.get(j + 1).trim().equals("VBN"))) {
					verbToChange = (phrase.get(j + 2));
					verbToChange = verbToChange.trim();
					break;
				}
			} 
		}
		
//		if (passiveTense == "PaPaCo") //verb to modify for past continuous passive 
//		{
//			for(int i=a;i>=subBoundryIndex;i--)
//			{
//				if(phrase.get(i).trim().equals("VBN")&& phrase.get(i-1).trim().equals("VP"))
//				{
//					verbToChange = phrase.get(i+1);
//					//changingWordIndex = i+1;
//					break;
//				}
//			}
//		}
		
		System.out.println("PastTense(passive) output :"+verbToChange);
		
		return verbToChange;
	}
	
}
